from jira import JIRA
import sys, logging

JIRA_PROD_SERVER = 'https://localhost'
JIRA_QA_SERVER = 'https://localhost'

# This script is for example purposes, as it depended on proprietary systems, 
# and all references to this had to be stripped out of the code. 

#variables for our flag setup
HAS_ISSUELINKS	= 1
HAS_EPICLINKS	= 2
HAS_REMOTELINKS = 4

#HAS_IL_AND_EL	= 3
#HAS_IL_AND_RL	= 5
#HAS_EL_AND_RL	= 6
#HAS_IL_EL_R		= 7

NO_SUBTASKS = False
SUBTASKS	= True

def create_issues(jira_obj, project, tech, std_error):

	#issue key translation table stuff
	dummy_dict = {}
	issuekeys_old_to_new = {}
	#issuekeys_old_to_new_temp = {}

	#these two issue creation lists will be done separately
	issue_list = []
	issue_list_subtasks = []

	#list of issue keys from the source project
	old_issuekeys = []

	#array of issues that are linked to other issues, or have remote links/urls
	issues_to_update = []

	#create jql query strings
	jql_string_no_subtasks = create_jql_string(tech, NO_SUBTASKS)
	jql_string_subtasks = create_jql_string(tech, SUBTASKS)
	std_error.write("Successfully created JQL strings from user's selections\n")
	# We have to do non-subtask queries first, because subtasks must be created
	# with a parent id AT creation time
	issue_list = issues_to_array(jira_obj, old_issuekeys, issues_to_update, \
										jql_string_no_subtasks, project, dummy_dict)
	std_error.write("First call to issues_to_array() completed\n")

	#run the split here
	broken_up_lists = breakup_list(issue_list)
	broken_up_old_issuekeys = breakup_list(old_issuekeys)

	for i in range(len(broken_up_lists)):
		try:
			created_issues = jira_obj.create_issues(field_list=broken_up_lists[i])
			#std_error.write(str(created_issues[0]))
		except Exception as error:
			std_error.write("Error: "+str(error)+"\n")
			traceback.print_exc(file=std_error)
			return error

		std_error.write("[no subtasks] bulk created an issue set of size: "+str(len(broken_up_lists[i]))+"\n")
		#std_error.write(str(created_issues[0]['input_fields']['summary']))
		#**** make the issue translation dictionary before the next bulk create ****
		issuekeys_old_to_new.update(make_translation_dict(broken_up_old_issuekeys[i], broken_up_lists[i], created_issues))
		std_error.write("[no subtasks] list of created issues added to translation table\n")

	#Now that we've created entries for all of the old issue keys in our
	#translation dict, clear the array (this is necessary for make_translation_dict
	# to function)
	old_issuekeys = []
	
	for key in issuekeys_old_to_new.keys():
		std_error.write(key)
	#Now run the issue creation for all subtasks below
	issue_list_subtasks = issues_to_array(jira_obj, old_issuekeys, \
								issues_to_update, jql_string_subtasks, \
								project, issuekeys_old_to_new)
	std_error.write("second call to issues_to_array() completed\n")

	broken_up_lists = breakup_list(issue_list_subtasks)
	broken_up_old_issuekeys = breakup_list(old_issuekeys)
	#send the bulk list of sub-tasks to Jira API

	for i in range(len(broken_up_lists)):
		try:
			created_issues = jira_obj.create_issues(field_list=broken_up_lists[i])
		except Exception as error:
			#propogate the error back to __init__.py
			return error
		std_error.write("[subtasks] bulk created an issue set of size: "+str(len(broken_up_lists[i]))+"\n")
		#update the issue translation table (not sure if we need to do this)
		issuekeys_old_to_new.update(make_translation_dict(broken_up_old_issuekeys[i], broken_up_lists[i], created_issues))
		std_error.write("[subtasks] list of created issues added to translation table\n")
	temp_dict = {}
	#add issue associations here
	for issue_obj in issues_to_update:
		#multiple loops to handle the different types of associated issues
		issue_str = issue_obj['issue']
		flag = issue_obj['flag']
		#grab the issue object
		old_issue_temp = jira_obj.search_issues("id="+issue_str)
		#get the single issue from returned array using helper f'n
		old_issue = get_single_issue(old_issue_temp)

		if flag & HAS_ISSUELINKS:
			#handle issuelinks. These currently aren't used so we'll want to warn
			#if any issues have this field
			for issue_link in old_issue.fields.issuelinks:
				#not sure there are any issuelinks fields which link to
				#actual issues and not just to external webpages
				il_dict = issuelinks_helper(issue_link)
				if not bool(il_dict):
					pass
				else:
					logging.warning("issuelinks found on issue key: "+issue_str)

		if flag & HAS_EPICLINKS:
			#There should only be one epic link in the child
			old_epic_id = old_issue.fields.customfield_10000
			new_epic_id = issuekeys_old_to_new[old_epic_id]
			#print(type(new_epic_id))
			temp_dict = { "customfield_10000": new_epic_id }
			#get the destination issue object
			issue_array = jira_obj.search_issues("id="+issuekeys_old_to_new[str(old_issue)])
			dest_issue = get_single_issue(issue_array)
			dest_issue.update(temp_dict)

		if flag & HAS_REMOTELINKS:
			logging.info("Adding remote links to issue: "+issue_str)
			#gather up remote links
			remote_links = jira_obj.remote_links(old_issue)
			rl = {}
			for r in remote_links:
				rl = { 'url': r.object.url, \
					 'title': r.object.title }
				# grab the new key for the issue we need to add remotelinks to
				new_issue_key = issuekeys_old_to_new[issue_str]
				#grab the issue object
				new_issue_temp = jira_obj.search_issues("id="+new_issue_key)
				#grab the single issue from returned array using helper f'n
				new_issue_obj = get_single_issue(new_issue_temp)
				jira_obj.add_simple_link(new_issue_obj, rl)

		if flag == 0:
			logging.warning("WARNING: issue "+issue_str+" wrongfully entered into issues_to_update")
			pass
	std_error.write("Finished job successfully\n")
	jira_obj.close()

#EFFECTS: creates a translation table which maps issue keys from the source proj
#		  to the corresponding key for that issue in the destination project.
#		  This is necessary for the creation of Sub-Tasks
def make_translation_dict(old_issuekeys, issue_list, jira_return_list):
	issue_translation_dict = {}
	#sanity check
	if len(old_issuekeys) != len(jira_return_list):
		return sys.stderr.write("Error: jira failed to create all desired issues")
	#create the actual dict
	for i in range(len(old_issuekeys)):
		#this check is for testing purposes
		if issue_list[i]['summary'] != jira_return_list[i]['input_fields']['summary']:
			return sys.stderr.write("issue_list and the list return by jira API do not have the same orderering")
		#create the dictionary entry
		issue_translation_dict[old_issuekeys[i]] = jira_return_list[i]['issue'].key
	return issue_translation_dict


#EFFECTS: Runs the JQL query, and duplicates all issues returned by the query into
#		  the "destination_project" parameter.  Additionally, this function creates
#		  the issue translation dictionary as well as a list of items that require
#		  the addition of remote links later on (would be nice if we didn't have
#		  to do this separately...).
def issues_to_array(jira, old_issuekeys, issues_to_update, jql_string, destination_project, issuekeys_dict):
	#send the query and get a result list of issues
	issues_to_create = []
	issue_query = jira.search_issues(jql_string, startAt=0, maxResults=False, validate_query=True)
	parent_id=""
	is_epic = False
	#This will eventually be user input, and we'll want to offer a retry rather
	# than simply bombing out of the program FIXME
	try:
		dev_proj = jira.project(destination_project)
	except:
		logging.error("The Project ("+ str(destination_project) +") specified does not exist.  Exiting..")
		return sys.stderr.write("The Project ("+ str(destination_project) +") specified does not exist.  Exiting..")
		exit(1)

	# Do first pass over all issues returned by the query,
	# create array of issue objects.  Then bulk create issues and create
	# translation table for mapping old issue keys
	# (keys the issues had in the project we're copying from)
	# to new ids created in the destination project
	temp_issue_to_update = {}
	for issue in issue_query:

		is_epic = False

		#print('DUPLICATING ISSUE: ', issue)
		issue_type = issue.fields.issuetype.name
		issue_type_id = issue.fields.issuetype.id
		if issue_type == 'Epic':
			epic_name = issue.fields.customfield_10002
			is_epic = True
		else:
			epic_name = None

		status_name = issue.fields.status.name

		#Check to see if we'll have to update this issue
		temp_issue_to_update['issue'] = str(issue)
		remote_links = jira.remote_links(issue)
		temp_issue_to_update['flag'] = set_update_flag(issue, remote_links)
		#record whether this issue will need to use the issue translation dict or
		# need the addition of remote links later. if it will, add it to the list
		if temp_issue_to_update['flag'] != 0:
			issues_to_update.append(temp_issue_to_update)
		temp_issue_to_update = {}

		if issue_type == "Sub-task":
			parent_id = issuekeys_dict[str(issue.fields.parent)]

		new_issue = make_issue_obj(jira, project=dev_proj, issue_type=issue_type, \
			  status=issue.fields.status, summary=issue.fields.summary, \
			  desc=issue.fields.description, due_date=issue.fields.duedate, \
			  versions=issue.fields.versions, priority=issue.fields.priority, \
			  fix_versions=issue.fields.fixVersions, labels=issue.fields.labels, \
			  components=issue.fields.components, reporter=issue.fields.reporter, \
			  issuelinks=issue.fields.issuelinks, epic_name=epic_name, \
			  epic_links=issue.fields.customfield_10000, type_id=issue_type_id, \
			  sprint=issue.fields.customfield_10004, parent_id=parent_id, \
			  story_points=issue.fields.customfield_10006)
		#add issue object to list
		issues_to_create.append(new_issue)
		old_issuekeys.append(str(issue))
		#update key translation table
		#issuekeys_dict[str(issue)] = str(new_id)
	return issues_to_create

def set_update_flag(issue, remote_links):
	temp_flag = 0
	#check issuelinks
	if len(issue.fields.issuelinks) == 0:
		pass
	else:
		temp_flag = temp_flag | HAS_ISSUELINKS
	#check epic links
	if issue.fields.customfield_10000 is None:
		pass
	elif len(issue.fields.customfield_10000) == 0:
		pass
	else:
		temp_flag = temp_flag | HAS_EPICLINKS

	if len(remote_links) != 0:
		temp_flag = temp_flag | HAS_REMOTELINKS

	return temp_flag


#DESCRIPTION: A simple helper which does some small checks. This function is needed because
# when we query jira for what we expect to be one issue, jira still returns a
# "resultList" which must be iterated, even though we are only expecting a single result.
# This is likely for conventions sake within jira code.  Thus this function is necessary.
# EFFECTS: checks to make sure the array passed as an argument has length == 1
def get_single_issue(issue_array):
	#sanity check
	if len(issue_array) != 1:
		logging.error("ERROR: Preexisting issue was deleted during the execution of this program")
		return sys.stderr.write("ERROR: Preexisting issue was deleted during the execution of this program")
	#grab the (single) issue and return it
	ret_issue = None
	for i in issue_array:
		ret_issue = i
	return ret_issue

def arg_check(arg, name):
	if isinstance(arg, int):
		if arg == -1:
			logging.error("must supply " + str(name) + " argument to make_issue_obj function")
			return sys.stderr.write("must supply " + str(name) + " argument to make_issue_obj function")
	else:
		return
#REQUIRES: Issue link is defined and passed from a jira issue object
#EFFECTS:  Constructs a dictionary populated with data from a single issue
#	   link
def issuelinks_helper(issue_link):

	il_dict = {}

	if issue_link is None:
		logging.warning("WARNING: Undefined issue link passed to issuelink_helper")
		return {}

	try:
		issue_link.inwardIssue
	except:
		pass
	else:
		#NOTE this issue may have to be converted to string and be set
		# by id
		il_dict["inwardIssue"] = issue_link.inwardIssue

	try:
		issue_link.outwardIssue
	except:
		pass
	else:
		il_dict["outwardIssue"] = issue_link.outwardIssue

	try:
		issue_link.relationship
	except:
		return il_dict
	else:
		il_dict["relationship"] = issue_link.relationship

	return il_dict


def make_issue_obj(jira, **keyword_args):
	#check required args
	project = keyword_args.pop('project', -1)
	arg_check(project, 'project')

	issue_type = keyword_args.pop('issue_type', -1)
	arg_check(issue_type, 'issue_type')

	status = keyword_args.pop('status', -1)
	arg_check(status, 'status')

	summary = keyword_args.pop('summary', -1)
	arg_check(summary, 'summary')

	description = keyword_args.pop('desc', -1) #this is an rtf field
	arg_check(description, 'desc')

	due_date = keyword_args.pop('due_date', -1)
	arg_check(due_date, 'due_date')

	affects_versions = keyword_args.pop('versions', -1)
	arg_check(affects_versions, 'versions')

	fix_versions = keyword_args.pop('fix_versions', -1)
	arg_check(fix_versions, 'fix_versions')

	components = keyword_args.pop('components', -1)
	arg_check(components, 'components')

	labels = keyword_args.pop('labels', -1)
	arg_check(labels, 'labels')

	priority = keyword_args.pop('priority', -1)
	arg_check(priority, 'priority')

	reporter = keyword_args.pop('reporter', -1)
	arg_check(reporter, 'reporter')

	sprint_field = keyword_args.pop('sprint', -1)
	arg_check(sprint_field, 'sprint')

	parent_id = keyword_args.pop('parent_id', -1)
	arg_check(parent_id, "parent id")

	type_id = keyword_args.pop('type_id', -1)
	arg_check(type_id, "issue type id")
	#optional args (may or may not be defined)

	epic_name = keyword_args.pop('epic_name', -1)

	epic_link = keyword_args.pop('epic_links', -1)
	if epic_link == -1:
		epic_link = None

	story_points = keyword_args.pop('story_points', None)
	#issuelinks = keyword_args.pop('issuelinks', None)
	#gather fixVersion names
	version_names = []
	for v in fix_versions:
		version_names.append({"id" : v.id})
	#gather components
	component_array = []
	for c in components:
		component_array.append({"name" : c.name})
	#gather "affects version" names
	affects_version_names = []
	for n in affects_versions:
		affects_version_names.append({"name" : n.name})

	if description is None:
		description = ""

	#issuelinks = issuelinks_helper(issuelinks)
	type_id_string = str(type_id)

	issue_dict = {
		'project': project.key,
		'summary': summary,
		#'status': status.name,
		'description': description,
		#'dueDate': due_date,
		#'versions': affects_version_names,
		#'fixVersions': version_names,
		#'components': component_array,
		'issuetype' : { 'id' : type_id_string },
		#'issuetype': {'name': issue_type},
		'priority': {'name': priority.name},
		#'reporter': {'name': reporter.name},  #omitted for now to prevent spamming 
		'labels': labels,
		#'customfield_10000': epic_link, #epic link is broken (links to issues in parent project rather than child)
		#'customfield_10006': story_points,
		#'customfield_10004': sprint_field, #we should also be able to use jira system field here
		#'issuelinks': issuelinks
	}

	if issue_type == 'Epic':
		issue_dict['customfield_10002'] = epic_name
	elif issue_type == 'Sub-task':
		issue_dict['parent'] = { "id": parent_id }

	return issue_dict

def create_jql_string(tech, subtasks):

	if subtasks:
		query_string = 'project = EXAMPLE AND issuetype = Sub-task'
		query_string += ' AND labels in ('
	else:
		query_string = 'project = EXAMPLE AND issuetype != Sub-task'
		query_string += ' AND issuetype != Initiative'
		query_string += ' AND labels in ('

	first_one = True
	for t in tech:
		if first_one:
			query_string += '\"'+ str(t) +'\"'
			first_one = False
		else:
			query_string += ', \"'+ str(t) +'\"'

	query_string += ')'
	return query_string


def breakup_list(issue_list):
	CHUNK_SIZE = 999
	return_list = []
 	#break up list of issues into chunks of size 999 or less
	for i in range(0, len(issue_list), CHUNK_SIZE):
		return_list.append(issue_list[i:i+CHUNK_SIZE])

	return return_list
