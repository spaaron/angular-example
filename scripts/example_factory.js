angular
	.module('example')
	.factory('example_factory', function($http){

		function get_proj_data() {
			return $http.get('https://localhost/test_backend');
		}

		function post_job_data(project, tech){
			var data = {
				project: project,
				technologies: tech
			}
			return $http.post('https://localhost/send_job', JSON.stringify(data));
		}

		return {
			get_proj_data: get_proj_data,
			post_job_data: post_job_data
		}
	});
