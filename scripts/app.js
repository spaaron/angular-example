var app = angular.module('example', []);

//change delimeters for angular variables to avoid conflict with jinja templates
//Delimeters will work as follows: {a  my_variable  a}
app.config(['$interpolateProvider', function($interpolateProvider){
  $interpolateProvider.startSymbol('{a');
  $interpolateProvider.endSymbol('a}');
}]);
