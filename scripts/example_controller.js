angular
	.module('example')
	.controller('example_controller', function($scope, example_factory){

		//angular.element(document).ready(function () {});

		//the KEY for the project currently selected
		$scope.selected_project = "";
		$scope.selected_technologies = [];
		//Makes project data gathered by example_factory to be accessed within html
		$scope.projects = [];
		$scope.project_warning = false;
		$scope.tech_warning = false;

		//array to manage window transitions
		$scope.active_window = [true, false, false, false];
		$scope.technologies = [{"name":"BASE" }, {"name":"example"}];

		//add the "selected" property to all objects in technologies array
		for(i in $scope.technologies){
			$scope.technologies[i].selected = true;
		}
		//state transition constants
		$scope.FORWARD = 0;
		$scope.BACKWARD = 1;
		//jumbotron transition constants
		$scope.START_SCREEN = 0;
		$scope.PROJ_SCREEN = 1;
		$scope.TECHNOLOGIES_SCREEN = 2;
		$scope.VERIFY_SCREEN = 3;

		//Call for our factory function which sources project data from Ford Jira instance
		example_factory.get_proj_data().then(function(response){
			$scope.projects = response.data;
			for(i in $scope.projects){
				//add the "selected" property to all objects in projects array
				$scope.projects[i].selected = false;
			}
		}, function error_callback(response){
			console.log(response);
		});

		//state transition function for window management --------------------------
		$scope.window_transition = function(state_num, direction){
			if(direction === $scope.FORWARD){
				if(state_num === $scope.PROJ_SCREEN && $scope.selected_project === ""){
					//don't allow forward state transition if there is no project selection
					$scope.project_warning = true;
					return;
				}
				else if(state_num === $scope.TECHNOLOGIES_SCREEN){
					//don't allow forward state transition if there is no technology selection
					$scope.selected_technologies = [];
					for(i in $scope.technologies){
						if($scope.technologies[i].selected === true){
							$scope.selected_technologies.push($scope.technologies[i].name)
						}
					}
					if($scope.selected_technologies.length === 0){
						$scope.tech_warning = true;
						return;
					}
				}
				else if(state_num === $scope.VERIFY_SCREEN){
					//Here we need to post our job data to the flask endpoint
					var proj = $scope.selected_project;
					var tech = $scope.selected_technologies;
					example_factory.post_job_data(proj, tech).then(function(response){
						console.log("Job data sent, received successfully by backend\n");
					}, function error(response){
						//activate an error dialogue and don't change screens
						console.log(response);
						return;
					});
				}
			}
			//disable the current screen
			$scope.active_window[state_num] = false;
			//determine direction of transition
			if(direction === $scope.BACKWARD){
				$scope.active_window[--state_num] = true;
			}
			else if(direction === $scope.FORWARD){
				$scope.active_window[++state_num] = true;
			}
		} //END window_transition --------------------------------------------------

		//debug print function -----------------------------------------------------
		$scope.print_debug = function(){
			//print the active window state array
			console.log($scope.active_window);
			console.log($scope.technologies);
			console.log($scope.projects);
		}
		//Sort function for project table data
		$scope.sort = function(keyname){
			$scope.sortKey = keyname;
			$scope.reverse = !$scope.reverse;
		}

		//tech selection function --------------------------------------------------
		$scope.tech_select = function(tech_name){
			$scope.tech_warning = false;
			var index = $scope.technologies.map(function(o) { return o.name; }).indexOf(tech_name);
			$scope.technologies[index].selected = !$scope.technologies[index].selected;
		} //END tech_select --------------------------------------------------------

		$scope.proj_select = function(proj_name){
			$scope.project_warning = false;
			var index = $scope.projects.map(function(o) { return o.name; }).indexOf(proj_name);
			console.log("clicked on project key: " + $scope.projects[index].key)
			if($scope.projects[index].selected){
				//simply deselect the current selection and return
				$scope.projects[index].selected = false;
				//This is a deselection, so clear the current selection variable
				$scope.selected_project = "";
				return;
			}
			//deselect all other projects
			for(i in $scope.projects){
				$scope.projects[i].selected = false;
			}
			//activate the selected project
			$scope.projects[index].selected = true;
			//update the current name selection variable
			$scope.selected_project = $scope.projects[index].key;
		}
	});
