#!/usr/bin/python3.4

import json, sys
from flask import Flask, render_template
from flask import url_for, request
from jira import JIRA

sys.path.append('/home/aaron')
from create_issues import create_issues

app = Flask(__name__)
app.debug = True
#work with ####'s Jira QA site for now
JIRA_SERVER = "https://www.jira.com"
#Anything that is written to stderr will appear in the examplesite-error log in /var/log/apache2
#sys.stderr.write("__init__ run \n")
file_obj = open("./.bad-pass-practice", "r")
auth_str = file_obj.readline(16)
file_obj.close()
auth_str = auth_str[:14]
#FIXME auth with personal account for now.  Eventually, we need a generic acc here.
jira_obj = JIRA(JIRA_SERVER, basic_auth=('aspe666', auth_str))
projects = jira_obj.projects()

#======================Begin Function endpoint definitions======================

@app.route("/", methods=['GET'])
def homepage():
	return render_template('index.html')


@app.route("/test_backend", methods=['GET'])
def projects_fetch():
	project_list = []
	for project in projects:
		project_list.append(create_proj_obj(project))
	return json.dumps(project_list)

@app.route("/send_job", methods=['POST'])
def duplicate_project():
	sys.stderr.write("project data received\n")
	json_data = request.get_json()
	debug_check = str(json_data['project'])
	if debug_check != "EXPLDEV":
		sys.stderr.write("ERROR: User is attempting to create issues in a dest project other than the DEV project...exiting...")
		exit(1)
	try:
		create_issues(jira_obj, json_data['project'], json_data['technologies'], sys.stderr)
	except Exception as error:
		sys.stderr.write(str(error))
		exit(1)

	return json.dumps({'success':True}), 200, {'ContentType':'application/json'}

if __name__ == "__main__":
	app.run()

def create_proj_obj(project):
	temp_obj = {"name": "", "key":"", "id": ""}
	temp_obj["name"] = project.name
	temp_obj["key"]  = project.key
	temp_obj["id"]   = project.id
	return temp_obj
