## Welcome to Aaron's angularJS starter page   

This tool, hosted as a website, used `create_issues.py` to effectively duplicate a set of issues in a Jira project, and create a replica project.

### Main Python script used:    
+ [create_issues.py](./create_issues.py)
+ [python init file](./\_\_init\_\_.py)

### Key AngularJS components:

js files:
+ [example_controller.js](./scripts/example_controller.js)
+ [example_factory.js](./scripts/example_factory.js)
+ [app.js](./scripts/app.js)   

html:
+ [index](./index.html)

### A Note to Developers

NOTE: Recreating the interactive site will be easiest on a recent version of OpenSUSE (15.3 for us), however there's no reason why it wouldn't work on Ubuntu Server with Apache2 installed either.  

#### Dependencies: 
+ Apache2
+ python-flask
+ AngularJS
+ mod_wsgi 
+ Bootstrap framework

Each dependency requires a small amount of config to get working   
